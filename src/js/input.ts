import * as $ from 'jquery';
import Bead from './bead';
import { exportPlate, importPlate } from './io';
import { PlateType, rect, verteces } from './plates';
import { IRendererOptions, Renderer } from './renderer';
import { Vector } from './utils';

export default function initEvents(
  rendererNames: string[],
  types: PlateType[],
  renderPlate: (type: PlateType, size: number, renderer: Renderer) => void,
  getRenderer: (name: string) => Renderer) {

  let selectedType: PlateType;
  let selectedRenderer: Renderer;

  $('#input-import').on('change', e => {
    importPlate((e.target as HTMLInputElement).files[0], (beads, colors) => {
      selectedRenderer.clear();
      selectedRenderer.beads = beads;
      selectedRenderer.colors = colors;
      selectedRenderer.render();
    });
  });

  $('#nav-item-export').on('click', e => {
    exportPlate(selectedRenderer);
  })

  const $sizeInput = $('#input-plate-size');
  $sizeInput.val(10);

  $('#input-plate-size-inc').on('click', e => {
    const val = parseInt($sizeInput.val(), 10);
    $sizeInput.val(val + 1);
  });

  $('#input-plate-size-dec').on('click', e => {
    const val = parseInt($sizeInput.val(), 10);
    if (val > 0) {
      $sizeInput.val(val - 1);
    }
  });

  function makeDropdown<T>(
      $dropdown: JQuery,
      $text: JQuery,
      values: T[],
      onSelection: (t: T) => void,
      describe: (t: T) => string = t => t + ''
    ) : void {

    values.forEach(t => {
      let text = describe(t);
      let $li = $(`<li><a href='#'>${text}</a></li>`);
      $dropdown.append($li);
      $li.data('value', t);
      $li.data('text', text);
    });

    $dropdown.on('click', 'li', e => {
      let $li = $(e.currentTarget);
      let t: T  = $li.data('value')
      $text.text($li.data('text'));
      onSelection(t);
      return true;
    });

    $dropdown.find('li:first-of-type').trigger('click');
  }

  makeDropdown(
    $('#dropdown-plate-type'),
    $('#plate-type-text'),
    types,
    type => selectedType = type,
    type => PlateType[type]
  );

  makeDropdown(
    $('#dropdown-renderer-type'),
    $('#renderer-type-text'),
    rendererNames,
    name => selectedRenderer = getRenderer(name)
  );


  $('#new-plate').on('click', e => {
    renderPlate(selectedType, $sizeInput.val(), selectedRenderer);
    return false;
  });

  const moveFactor: number = 4;
  const $body = $('body');
  $body.on('keyup', e => {
    switch(e.key) {
      case 'ArrowUp': {
        selectedRenderer.move(0, -1 * moveFactor);
        break;
      }
      case 'ArrowRight': {
        selectedRenderer.move(1 * moveFactor, 0);
        break;
      }
      case 'ArrowDown': {
        selectedRenderer.move(0, 1 * moveFactor);
        break;
      }
      case 'ArrowLeft': {
        selectedRenderer.move(-1 * moveFactor, 0);
        break;
      }
    }
  });

  const $main = $('#main');
  let isdown = false;
  let selection: Vector;
  let startX :number;
  let startY :number;

  $body.on('mousedown', e => {
    if(e.button == 0 && (e.target.id == 'main' || $main.has(e.target).length)){
      isdown = true;
      selectedRenderer.onSelectionChanged(Vector.empty);
      selection = new Vector(startX = e.clientX, startY = e.clientY, e.clientX, e.clientY);
    }
  });

  $body.on('mousemove', e => {
    if(isdown){

      selection.x0 = startX;
      selection.y0 = startY;
      selection.x1 = e.clientX;
      selection.y1 = e.clientY;

      selection.normalize();
      
      selectedRenderer.onSelectionChanged(selection);
    }
  });

  $body.on('mouseup click', e => {
    if(isdown){
      isdown = false;
      selection = null;
      selectedRenderer.onSelectionChanged(selection);
      e.preventDefault();
    }
  });
}

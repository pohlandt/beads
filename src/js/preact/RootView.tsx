import { h, render, Component } from 'preact';
import { IRendererOptions } from '../renderer';
import Bead from '../bead';
import BeadView from './BeadView';
import { Vector } from '../utils';
import * as $ from 'jquery';

export interface RootViewProps {
	opts: IRendererOptions
	onBeadClicked: (bead: Bead) => void;
}

interface RootViewState {
	beads: Bead[];
	colors: string[];
}

export default class RootView extends Component<RootViewProps, RootViewState> {

	private selectionElement: JQuery;

	constructor() {
		super();
		this.setState({ beads: [], colors: [] });
	}

	render(props, state) {
		return 	<div class={ props.opts.position ? 'bead-positioner' : '' }>
					{
						state.beads.map((bead) => (<BeadView bead={bead} ref={v => bead.view = v} colors={state.colors} rootProps={props} />))
					}
				</div>;
	}

	public onSelectionChanged(selection: Vector) : void {
		if(selection){
			this.state.beads.forEach(b => {
				(b as any).view.onSelectionChanged(selection);
			});
			
			if(!this.selectionElement){
				this.selectionElement = $('<div />').appendTo($('body'));
			}

			this.selectionElement.css({
				position: 'fixed',
				top: (selection.y0) + 'px',
				left: (selection.x0) + 'px',
				width: (selection.x1 - selection.x0) + 'px',
				height: (selection.y1 - selection.y0) + 'px',
				border: '1px solid lightblue'
			});
		}
		else{
			this.selectionElement && this.selectionElement[0].removeAttribute('style');
		}
	}
}

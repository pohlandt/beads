import { h, render, Component } from 'preact';
import Bead from '../bead';
import { RootViewProps } from './RootView';
import { Vector } from '../utils';

interface BeadViewProps {
	rootProps: RootViewProps;
	bead: Bead;
	colors: string[];
}

const emptyColor = Bead.emptyColor();

export default class BeadView extends Component<BeadViewProps, any> {
	private e: Element;

	constructor(props: BeadViewProps) {
		super(props);
	}

	componentDidMount() {
	}

	componentWillUnmount() {
	}

	render(props, state) {
		const b = props.bead;
		return 	<div ref={v => this.e = v } onClick={() => props.rootProps.onBeadClicked(b)} style={this.getStyle(b, props.colors, props.rootProps.opts.position)} class={`bead${b.color == emptyColor ? ' empty' : ''}${b.selected ? ' selected' : ''}`}>
					<div></div>
				</div>;
	}

	getStyle(bead: Bead, colors: string[], position: boolean) : string {
		const pos: string = position ? `left:${bead.x}px;top:${bead.y}px;` : '';
		if(bead.color == emptyColor){
			return pos;
		}

		return pos + `background:${colors[bead.color as number]};`;
	}

	public onSelectionChanged(selection: Vector) : void {
		const pos = this.e.getBoundingClientRect();
		const selected = selection && 
			selection.x0 < pos.right && 
			selection.x1 > pos.left &&
			selection.y0 < pos.bottom && 
			selection.y1 > pos.top;
		if(this.props.bead.selected != selected){
			this.props.bead.selected = selected;
			this.forceUpdate();
		}
	}
}

import * as $ from 'jquery';
import Bead from './bead';
import { Vector } from './utils';

export interface IRendererOptions {
    color?: boolean;
    position?: boolean;
}

const defaultOptions: IRendererOptions = {
  color: true,
  position: true,
};

export class Renderer {
    public beads: Bead[];
    public onBeadClicked: (b: Bead) => void;
    public colors: string[];

    protected element: Element;
    protected opts: IRendererOptions;

    constructor(opts?: IRendererOptions) {
        this.opts = Object.assign({}, defaultOptions, opts);
    }

    public attach(document: Document, parent?: Element, id?: string): Element {
        id = id || 'renderer';
        let e = document.getElementById(id);
        if (e == null) {
            (parent || document.body).appendChild(e = document.createElement('div'));
            e.id = id;
        }

        return this.element = this.attachToElement(e);
    }

    public clear(): void {
        if (this.element) {
            while (this.element.hasChildNodes()) {
                this.element.removeChild(this.element.childNodes[0]);
            }
        }

        this.beads = null;
    }

    public detach(): void {
        if (this.element) {
            const e = this.element;
            if (e && e.parentElement) {
                e.parentElement.removeChild(e);
            }
            this.element = null;
        }

        this.beads = null;
    }

    public move(x: number, y: number): void {
      throw new Error('not implemented');
    }

    public render(): void {
        throw new Error('not implemented');
    }

    protected attachToElement(e: Element): Element {
        return e;
    }

    public onSelectionChanged(selection: Vector): void {
        throw new Error('not implemented');
    }
}

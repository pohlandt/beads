var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var tsify = require('tsify');
var babelify = require('babelify');
var less = require('gulp-less');
var path = require('path');
var webserver = require('gulp-webserver');

var target = './target';

var bundler = browserify()
  .add('src/js/index.ts')
  .plugin(tsify, { target: 'es6' })
  .transform(babelify, {
    extensions: [ '.tsx', '.ts' ],
    plugins: [
      ['transform-react-jsx', { pragma:'h' }]
    ]
  });

function compile() {
    return bundler.bundle()
      .on('error', function(err) { console.error(err); this.emit('end'); })
      .pipe(source('build.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(target + '/js'));
}

function watch(){
  gulp.watch(['src/index.html'], ['html']);
  gulp.watch(['src/css/**'], ['less']);

  bundler = watchify(bundler);
  bundler.on('update', function() {
    console.log('-> update');
    compile();
  });
  compile();
}

gulp.task('html', function() {
  console.log('-> copying html');
  gulp.src(['src/index.html'], {base: 'src'}).pipe(gulp.dest(target));
});

gulp.task('libs', function() {
  console.log('-> copying libs');
  gulp.src(['node_modules/bootstrap/dist/css/*.min.css'], {base: 'node_modules/bootstrap/dist'}).pipe(gulp.dest(target));
  gulp.src(['node_modules/bootstrap/dist/fonts/**'], {base: 'node_modules/bootstrap/dist'}).pipe(gulp.dest(target));
  gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js'], {base: 'node_modules/bootstrap/dist/js'}).pipe(gulp.dest(target + '/vendor'));
  gulp.src(['vendor/**'], {base: '.'}).pipe(gulp.dest(target));
});

gulp.task('less', function () {
  console.log('-> less');
  return gulp.src('src/css/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest(path.join(target,'/css')));
});

gulp.task('serve', ['watch'], function() {
  gulp.src('target')
    .pipe(webserver({
      livereload: true,
      fallback: 'index.html',
      open: true
    }));
});

gulp.task('assets', ['html', 'less', 'libs']);
gulp.task('build', ['assets'], compile);
gulp.task('watch', ['assets'], watch);

gulp.task('default', ['watch']);

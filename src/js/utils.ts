import * as $ from 'jquery';

declare var window: any;

export const colors = ['black', 'blue', 'green', 'orange', 'pink', 'purple', 'red', 'yellow', '#FFC445', '#E54AEC', '#74F078', 'white'];

export class Vector {
  x0: number; y0: number; x1: number; y1: number;

  constructor(x0: number, y0: number, x1: number, y1: number){
    this.x0 = x0;
    this.y0 = y0;
    this.x1 = x1;
    this.y1 = y1;
  }

  public normalize() : Vector {
    if(this.x0 > this.x1){
      [this.x0, this.x1] = [this.x1, this.x0];
    }
    if(this.y0 > this.y1){
      [this.y0, this.y1] = [this.y1, this.y0];
    }

    return this;
  }

  static empty: Vector = new Vector(0, 0, 0, 0);
}

export function timed<T>(f: () => T, desc: string): T {
    const start = Date.now();
    const res = f();
    const ms = Date.now() - start;
    console.log(`${desc} took ${ms}ms`);
    return res;
}

export function timedRender<T>(f: () => T, desc: string): T {
    if(!window.requestIdleCallback){
      return f();
    }

    const start = Date.now();
    window.requestIdleCallback(() => {
      const ms = Date.now() - start;
      console.log(`${desc} took ${ms}ms`);
    });
    const res = f();
    return res;
}

import * as lf from 'localforage';
import { Renderer } from './renderer';
import Bead from './bead';
import * as utils from './utils';
import * as $ from 'jquery';

const currentVersion: number = 1;

const beadPropsToSerialize = ['x', 'y', 'color', 'selected', 'highlighted'];

class Storable {
  constructor(public beads: any, public colors: string[], public version: number) {}

  public stringify(): string {
    return JSON.stringify(this);
  }
}

export function importPlate(file: File, callback: (beads: Bead[], colors: string[]) => void): void {
    const fr = new FileReader();
    fr.addEventListener('loadend', () => {
      const parsed: any = JSON.parse(fr.result);
      let beads: Bead[];
      let colors: string[]
      if (typeof parsed == 'string'){
          beads = JSON.parse(parsed); // altes format
          colors = utils.colors;
      }
      else {
        beads = parsed.beads; // neues format
        colors = parsed.colors;
      }

      beads = beads.map(afterDeserialization);
      callback(beads, colors);
    });
    fr.readAsText(file);
}

export function exportPlate(r: Renderer) {
    if (!(r && r.beads && r.beads.length)) {
        alert('nothing to export');
        return;
    }

    const obj = { beads: r.beads.map(beforeSerialization), colors: r.colors };
    download(obj, 'beads.json');
}

function deserialize(stringOrArray: string | string[]): Bead[] {
    let beads;
    if (typeof stringOrArray === 'string') {
        beads = JSON.parse(stringOrArray as string);
    } else if ($.isArray(stringOrArray)) {
        beads = stringOrArray;
    }

    if (!$.isArray(beads)) {
        throw new Error('expected array or stringified array');
    }

    return beads && beads.map(b => afterDeserialization(b));
}

function afterDeserialization(bead: any): Bead {
    const b = new Bead();
    beadPropsToSerialize.forEach(key => {
        if (key === 'color') {
            b.color = bead.color === 'e' ? Bead.emptyColor() : bead.color as number;
        } else {
            b[key] = bead[key];
        }
    });

    return b;
}

function beforeSerialization(bead: Bead): Bead {
    const clone: any = $.extend(true, {}, bead);
    Object.keys(bead).forEach(key => {
        if (beadPropsToSerialize.indexOf(key) === -1) {
            delete clone[key];
        }
    });

    if (clone.color === Bead.emptyColor()) {
        clone.color = 'e';
    }

    return clone;
}

function download(beads: any, filename: string) {
    const s = JSON.stringify(beads);
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(new Blob([s], { type: 'application/octet-stream;charset=utf-8;' }), filename);
        return;
    }

    const link = document.createElement('a');
    link.download = filename;
    link.href = 'data:application/octet-stream;charset=utf-8,'
        + encodeURIComponent(s);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

export function savePlate(key: string, beads: Bead[], colors: string[],  success?: () => void, error?: (err: any) => void) {
  lf.setItem(key, new Storable(beads, colors, currentVersion).stringify()).then(err => {
    if (err) {
      if (error) {
        error(err);
      }
    } else {
      if (success) {
        success();
      }
    }
  });
}

export function loadPlate(r: Renderer, key: string, success?: () => void, error?: (err: any) => void) {

}

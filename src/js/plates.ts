import { Vector } from './utils';

export enum PlateType {
  round,
  rect,
  heart
}

interface Point {
    x: number; y: number;
}

export function verteces(type: PlateType, size: number, diameter: number, spacing: number) : Point[] {
  switch (type) {
      case PlateType.round: {
          return round(size, diameter, spacing);
      }
      case PlateType.rect: {
          return rect(size, size, diameter, spacing);
      }
      case PlateType.heart: {
        return heart(size, diameter, spacing);
      }
      default: {
          throw Error('wut?');
      }
  }
}

function rotate(v: Vector, cos: number, sin: number) : void {
    const nx = (cos * (v.x1 - v.x0)) + (sin * (v.y1 - v.y0)) + v.x0;
    const ny = (cos * (v.y1 - v.y0)) - (sin * (v.x1 - v.x0)) + v.y0;
    v.x1 = nx;
    v.y1 = ny;
}

function addPolyVertices(radius: number, n : number, vertices: Point[]) : void {
    if (n <= 1) {
        vertices.push({ x: 0, y: 0 });
        return;
    }

    const rad = (2 * Math.PI / n);
    const cos = Math.cos(rad);
    const sin = Math.sin(rad);
    const vec = new Vector(0, 0, radius * -1, 0);

    for (let i = 1; i <= n; i++) {
        vertices.push({ x: vec.x1, y: vec.y1 });
        rotate(vec, cos, sin);
    }
}

export function round(ringCount: number, thingDiameter: number, thingHorMargin: number) : Point[] {
    const n = ringCount || 1;
    let widthInThings = ((n - 1) * 2) + 1;
    let width = widthInThings * thingDiameter + (widthInThings - 1) * thingHorMargin;
    const vertices = [];
    for (let i = 1; i <= n; i++) {
        const thingCount = i === 1 ? 1 : (i - 1) * 6;
        widthInThings = ((i - 1) * 2) + 1;
        width = (widthInThings - 1) * (thingHorMargin + thingDiameter);
        addPolyVertices(width / 2, thingCount, vertices);
    }

    return vertices;
}

export function rect(widthInThings : number, heightInThings : number, thingDiameter : number, thingSpacing : number) : Point[] {
    const width = widthInThings * thingDiameter + (widthInThings - 1) * thingSpacing;
    const height = heightInThings * thingDiameter + (heightInThings - 1) * thingSpacing;
    const vertices = [];
    let cx = -1 * width / 2 + thingDiameter / 2;
    const startY = -1 * height / 2 + thingDiameter / 2;
    let cy = startY;
    for (let x = 1; x <= widthInThings; x++) {
        for (let y = 1; y <= heightInThings; y++) {
            vertices.push({ x: cx, y: cy });
            cy += thingDiameter + thingSpacing;
        }
        cy = startY;
        cx += thingDiameter + thingSpacing;
    }
    return vertices;
}

function move(points : Point[], dx: number, dy: number) : Point[] {
    points.forEach(p => {
        p.x += dx;
        p.y += dy;
    });

    return points;
}

function getDimensions(points: Point[], diameter: number) : Point {
    let maxX: number;
    let maxY: number;
    let minX: number;
    let minY: number;
    points.forEach(p => {
        maxX = typeof maxX == 'undefined' ? p.x : Math.max(maxX, p.x);
        maxY = typeof maxY == 'undefined' ? p.y : Math.max(maxY, p.y);
        minX = typeof minX == 'undefined' ? p.x : Math.min(minX, p.x);
        minY = typeof minY == 'undefined' ? p.y : Math.min(minY, p.y);
    });

    return {
        x: maxX - minX + diameter,
        y: maxY - minY + diameter
    };
}

export function heart(size: number, diameter: number, spacing: number) : Point[] {
    size = Math.max(size, 6);
    let result : Point[] = [];
    const heightInThings = size;
    const widthInThings = size + 1;
    let yOff = 0;
    for(let rowIndex: number = 0; rowIndex < heightInThings; rowIndex++){
        const rowWidth = 2 * rowIndex + 1;
        const exCenterCount = Math.floor(rowWidth / 2); 
        for(let colIndex = 0; colIndex < rowWidth; colIndex++){
            let relPos = colIndex - exCenterCount;
            result.push({ x: (diameter + spacing) * relPos, y: yOff });
        }

        yOff -= diameter + spacing;
    }

    let triangleDims = getDimensions(result, diameter);
    let maxWidthInThings = heightInThings * 2 - 1;

    let addHalfRound = function(left: boolean) {
        let ringCount = Math.floor(maxWidthInThings / 4) + 1;
        let halfRound = round(ringCount, diameter, spacing)
            .filter(p => p.y <= 0.1); // warum funktioniert <= 0 nicht ???
        let halfRoundDims = getDimensions(halfRound, diameter);
        let xOff = (triangleDims.x / 2) - (halfRoundDims.x / 2);
        xOff += (size % 2 == 1 ? 1 : -0.5) * (diameter + spacing) / 2
        halfRound = move(halfRound, 
            (left ? -1 : 1) * xOff, 
            -1 * (triangleDims.y + spacing));
        result = result.concat(halfRound);
    };

    addHalfRound(true);
    addHalfRound(false);

    let totalHeight = getDimensions(result, diameter).y;

    return move(result, 0, totalHeight / 2);
}

import * as $ from 'jquery';
import Bead from './bead';
import { IRendererOptions, Renderer } from './renderer';

const style = `
.bead-positioner {
	position: relative;
	left: 50%;
	top: 50%;
	height: 0px;
	width: 0px;
}

.bead {
	position: relative;
	border-radius: 50%;
}

.bead-positioner .bead, .bead > div {
	border-radius: 50%;
	position: absolute;
}

.bead > div {
	background: white;
}

.bead.empty, .bead.empty > div {
	border: 1px dotted #444444;
}

.bead:hover, .bead.empty:hover {
	border: 1px solid black;
}

.bead:hover > div {
	margin -1px 0 0 -1px;
}

.bead:not(.empty) {
	box-shadow: 1px 1px #444444;
}

.bead:not(.empty) > div {
	box-shadow: -1px -1px #444444;
}
`;

const emptyColor = Bead.emptyColor();

export default class DomRenderer extends Renderer {

    private beadDiameter: number;
    private beadDiameterUnit: string;
    private document: Document;
    private styleElement: Element;
    private beadToElement: Map<Bead, Element> = new Map();
    private movedX: number = 0;
    private movedY: number = 0;

    constructor(colors: string[], beadDiameter: number, beadDiameterUnit: string, opts?: IRendererOptions) {
        super(opts);
        this.colors = colors;
        this.beadDiameter = beadDiameter || 20;
        this.beadDiameterUnit = beadDiameterUnit || 'px';
    }

    public attach(document: Document, parent?: Element, id?: string): Element {
        const result = super.attach(document, parent, id);
        this.document = document;
        const e = document.createElement('style');
        e.id = 'dom-renderer-styles';
        e.textContent = style;
        document.head.appendChild(e);
        this.styleElement = e;
        return result;
    }

    public detach(): void {
        super.detach();

        if (this.styleElement) {
            const e = this.styleElement;
            if (e && e.parentElement) {
                e.parentElement.removeChild(e);
            }

            this.styleElement = null;
        }
    }

		public render(): void {
			const dirtyBeads = this.beads && this.beads.filter(b => b.isDirty());
			if (dirtyBeads) {
					dirtyBeads.forEach(b => this.renderBead(b));
			}
		}

    public move(x: number, y: number): void {
      this.movedX += x;
      this.movedY += y;
      $(this.element).css('transform', `translateX(${this.movedX}px) translateY(${this.movedY}px)`);
    }

    protected attachToElement(e: Element): Element {
        if (this.opts.position) {
            const c = document.createElement('div');
            e.appendChild(c);
            e = c;
            e.classList.add('bead-positioner');
        }

        $(e).on('click', '.bead', ev => {
            const clicked = document.elementFromPoint(ev.clientX, ev.clientY);
            this.onElementClicked(clicked);
        });

        return e;
    }

    protected onElementClicked(e: Element): void {
        let $e = $(e);
        if (!$e.is('.bead')) {
            $e = $e.parent();
        }
        e = $e[0];
        const bead = this.beads.find(b => this.beadToElement.get(b) === e);
        if (bead && this.onBeadClicked) {
            this.onBeadClicked(bead);
        }
    }

    protected renderBead(bead: Bead): void {
        let e: Element = this.beadToElement.get(bead);
        if (e) {
            e.parentElement.removeChild(e);
        }

        if (bead.color === emptyColor) {
            e = this.empty(bead.x, bead.y);
        } else {
            e = this.filled(bead.x, bead.y, this.colors[bead.color as number]);
        }

        this.beadToElement.set(bead, e);
        bead.renderedColor = bead.color;
    }

    protected nested(outerCss: string, innerCss: string, outerClass: string): Element {
        const outer = this.document.createElement('div');
        outer.setAttribute('style', outerCss);
        outer.setAttribute('class', outerClass);
        this.element.appendChild(outer);

        const inner = this.document.createElement('div');
        inner.setAttribute('style', innerCss);
        outer.appendChild(inner);
        return outer;
    }

    protected empty(x: number, y: number): Element {
        return this.nested(this.css(x, y), this.css(), 'bead empty');
    }

    protected filled(x: number, y: number, color: string): Element {
        return this.nested(this.css(x, y, color),
            this.css(undefined, undefined, 'white'), 'bead');
    }

    private css(x?: number, y?: number, color?: string): string {
        let diameter: number;
        let left: number;
        let top: number;
        let visual: string;
        let borderwidth: number;
        borderwidth = color ? 0 : 1;
        let renderPos = this.opts.position;

        let positionUnit = this.beadDiameterUnit;

        if ((typeof x) === 'undefined') {
            renderPos = true;
            const innerDiameter = this.beadDiameter / 2.5;
            const innerOffset = (this.beadDiameter - innerDiameter) / 2 - borderwidth;
            left = top = innerOffset;
            diameter = innerDiameter;
        } else {
            left = x;
            top = y;
            diameter = this.beadDiameter;
            positionUnit = 'px';
        }

        if (color) {
            visual = this.opts.color ? `background: ${color};` : '';
        } else {
            visual = `border-width: ${borderwidth}px;`;
        }

        return (renderPos ? `left: ${left}${positionUnit}; top: ${top}${positionUnit};` : '') + `height: ${diameter}${this.beadDiameterUnit}; width: ${diameter}${this.beadDiameterUnit};${visual}`;
    }
}

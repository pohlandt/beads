declare var window: any;

import * as $ from 'jquery';
import Bead from './bead';
import DomRenderer from './dom-renderer';
import PreactRenderer from './preact-renderer';
import initEvents from './input';
import { PlateType, rect, verteces } from './plates';
import { Renderer, IRendererOptions } from './renderer';
import { timedRender } from './utils';

const colorSelectionDiameter: number = 5;
const colorSelectionDiameterUnit: string = 'rem';
const beadDiameter: number = 40;
const beadDiameterUnit: string = 'px';
const colors = ['black', 'blue', 'green', 'orange', 'pink', 'purple', 'red', 'yellow', '#FFC445', '#E54AEC', '#74F078', 'white'];

function createRenderer(): Renderer {
    return new PreactRenderer(colors, beadDiameter, beadDiameterUnit, { position: true });
}

function createSelectionRenderer(): Renderer {
  return new PreactRenderer(colors, colorSelectionDiameter, colorSelectionDiameterUnit, { position: false });
}

function randomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function renderPlate(type: PlateType, size: number, renderer: Renderer): void {
    const nodes = verteces(type, size, beadDiameter, 5);
    const beads = nodes.map(v => new Bead(v.x, v.y));
    show(beads, renderer);
}

function show(beads: Bead[], renderer: Renderer): void {
    timedRender(() => {
      renderer.clear();
      renderer.beads = beads;
      renderer.render();
    }, `render by ${beadRenderer.constructor.name}`);
}

let selectedColor;
const layoutElement: Element = document.getElementById('layout');
const colorControlsElement = document.getElementById('color-controls');
const rendererNames: string[] = ['preact','vanilla-dom'];

const types: PlateType[] = [PlateType.heart, PlateType.round, PlateType.rect];

const selectionRenderer = createSelectionRenderer();
selectionRenderer.onBeadClicked = bead => {
    selectedColor = bead.color;
    if(beadRenderer.beads && beadRenderer.beads.length) {
      const selected = beadRenderer.beads.filter(b => b.selected);
      if(selected.length){
        selected.forEach(b => b.color = selectedColor);
        beadRenderer.render();
      }
    }
};

let i: number = 0;
selectionRenderer.beads = rect(1, colors.length, beadDiameter, 5)
    .map(v => new Bead(v.x, v.y, i++));
selectionRenderer.attach(document, colorControlsElement, 'bead-selection');
selectionRenderer.render();



let beadRenderer: Renderer;
let beadRendererName: string;
function getRenderer(name: string): Renderer {
  if(name === beadRendererName){
    return beadRenderer;
  }

  console.log('Setting renderer to', name);
  beadRendererName = name;
  if(beadRenderer){
    beadRenderer.detach();
  }

  switch(name){
    case rendererNames[1]: {
      beadRenderer = new DomRenderer(colors, beadDiameter, beadDiameterUnit, { position: true });
      break;
    }
    case rendererNames[0]: {
      beadRenderer = new PreactRenderer(colors, beadDiameter, beadDiameterUnit, { position: true });
      break;
    }
  }

  beadRenderer.onBeadClicked = bead => {
      if (typeof selectedColor === 'number') {
          bead.color = selectedColor;
          beadRenderer.render();
      }
  };

  beadRenderer.attach(document, layoutElement, 'main');

  return beadRenderer;
}

initEvents(rendererNames, types, renderPlate, getRenderer);
window.jQuery = $;

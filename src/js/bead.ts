const emptyColor = Symbol('emptyColor');

export default class Bead {
    public static emptyColor(): symbol {
        return emptyColor;
    }

    public x: number;
    public y: number;
    public color: symbol | number;
    public renderedColor: symbol | number;
    public selected: boolean;
    public highlighted: boolean;

    constructor(x?: number, y?: number, color: symbol | number = emptyColor) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.renderedColor = null;
        this.selected = false;
        this.highlighted = false;
    }

    public isDirty(): boolean {
        return this.color !== this.renderedColor;
    }
}

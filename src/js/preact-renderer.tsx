import Bead from './bead';
import { IRendererOptions, Renderer } from './renderer';
import RootView from './preact/RootView';
import { h, render, Component } from 'preact';
import { Vector } from './utils';

export default class PreactRenderer extends Renderer {

    private rootView : RootView;
    private beadDiameter: number;
    private beadDiameterUnit: string;
    private document: Document;
    private styleElement: Element;
    private beadToElement: Map<Bead, Element> = new Map();
    private movedX: number = 0;
    private movedY: number = 0;

    constructor(colors: string[], beadDiameter: number, beadDiameterUnit: string, opts?: IRendererOptions) {
        super(opts);
        this.colors = colors;
        this.beadDiameter = beadDiameter || 20;
        this.beadDiameterUnit = beadDiameterUnit || 'px';
    }

    public attach(document: Document, parent?: Element, id?: string): Element {
        const result = super.attach(document, parent, id);
        this.document = document;
				if(!document.querySelector('head > #preact-bead-styles')){
					render(<style id="preact-bead-styles">{
						`
						.bead-positioner {
							position: relative;
							left: 50%;
							top: 50%;
							height: 0px;
							width: 0px;
						}

						.bead {
							position: relative;
							border-radius: 50%;
              height: 40px;
              width: 40px;
						}

            .bead > div {
              height: 16px;
              width: 16px;
              left: 12px;
              top: 12px;
            }

            .bead.empty {
              border-width: 1px;
              opacity: 0.33;
            }

            .bead.empty > div {
              left: 11px;
              top: 11px;
              background: white;
              border-width: 1px;
            }

						.bead-positioner .bead, .bead > div {
							border-radius: 50%;
							position: absolute;
						}

            .bead.selected::before {
              display: block;
              height: 100%;
              width: 100%;
              border: 1px dashed red;
              content: '';
            }

						.bead > div {
							background: white;
						}

						.bead.empty, .bead.empty > div {
							border: 1px solid black;
						}

						.bead:hover, .bead.empty:hover {
              border: 1px solid black;
              opacity: 1;
						}

						.bead:hover:not(.empty) > div {
							margin: -1px 0 0 -1px;
						}

						.bead:not(.empty) {
							box-shadow: 1px 1px #444444;
						}

						.bead:not(.empty) > div {
							box-shadow: -1px -1px #444444;
						}
						`
					}</style> , document.head);
				}

        return result;
    }

    public move(x: number, y: number): void {
      this.movedX += x;
      this.movedY += y;
      $(this.element).css('transform', `translateX(${this.movedX}px) translateY(${this.movedY}px)`);
    }

    protected attachToElement(e: Element): Element {
      render(<RootView ref={(v) => this.rootView = v } opts={this.opts} onBeadClicked={ (b: Bead) => { this.onBeadClicked(b) } } />, e);
			return e;
    }

		public render():  void {
			console.log('rendering', this.beads.length, 'beads');
      this.rootView.setState({ beads: this.beads, colors: this.colors });
		}

    public clear(): void {
        this.rootView.setState({ beads: [], colors: this.colors });
    }

    public onSelectionChanged(selection: Vector) : void {
      this.rootView.onSelectionChanged(selection);
    }
}
